using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.Models;

namespace Catalog.Repositories 
{

    public class InMemItemsRepository : IItemsReposiory
    {
        private readonly List<Item> Items = new()
        {
            new Item { Id = Guid.NewGuid(), Name = "Potion", Price = 9, CreatedDate = DateTimeOffset.UtcNow },
            new Item { Id = Guid.NewGuid(), Name = "Iron Sword", Price = 19, CreatedDate = DateTimeOffset.UtcNow },
            new Item { Id = Guid.NewGuid(), Name = "Bronze Shield", Price = 15, CreatedDate = DateTimeOffset.UtcNow }
        };

        public async Task<IEnumerable<Item>> GetItemsAsync()
        {
            return await Task.FromResult(this.Items);
        }

        public async Task<Item> GetItemAsync(Guid id)
        {
            var items = this.Items.Where(item => item.Id == id).SingleOrDefault();
            return await Task.FromResult(items);
        }

        public async Task CreateItemAsync(Item item)
        {
            this.Items.Add(item);
            await Task.CompletedTask;
        }

        public async Task UpdateItemAsync(Item item)
        {
            var index = Items.FindIndex(existingItem => existingItem.Id == item.Id);
            Items[index] = item;
            await Task.CompletedTask;
        }

        public async Task DeleteItemAsync(Guid Id)
        {
            var index = Items.FindIndex(existingItem => existingItem.Id == Id);
            Items.RemoveAt(index);
            await Task.CompletedTask;
        }
    }
}